<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <div class="wrap WPRESTO col-md-12">
        <h2 class="left">Restaurant Menu Manager - Setup Categories</h2>
        <div class="clear"></div>
        <hr />

        <?php


        if(isset($_POST['wpresto_delete_cat']['category']))
        {
          $del_cat_id = $_POST['wpresto_delete_cat']['category_id'];
          $cat = new WPRESTO_Category();
          $cat->destroy($del_cat_id);
          echo wpresto_alert_msg("Item <b>" . $_POST['wpresto_delete_cat']['name'] . "</b> successfully deleted! ");
        }
        $menu_id = $_GET['menu_id'];
        $menu = new WPRESTO_Menu((int) $menu_id);
        $category = new WPRESTO_Category();
        if ($menu_id){
          $categories = $category->get_all_by_menu($menu_id);
        }else{

          $categories = $category->get_all();
        }
        //$categories = $category->get_by_id();
        //echo "cat: " . $categories;
        //print_r($categories);
        ?>

          <div class="navi sticky-top">
              <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
              <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
              <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
          </div>
          <br>

        <table class="widefat table table-hover">
          <thead>
          <tr>
            <th>名称</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Parent Menu</th>
            <th>Order</th>
            <th>Active</th>
            <th>ID</th>
            <th>Delete</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          </tfoot>
          <tbody>
          <?php foreach($categories as $c): ?>
            <tr>
                <td><strong><?php echo $c->name_cn; ?></strong></td>
              <td><strong><?php echo $c->name; ?></strong></td>
              <td><a href="<?php echo home_url(); ?>/edit-category?cat_id=<?php echo $c->id; ?>">Edit Category</a></td>
              <td><?php
                $pmenu = new WPRESTO_Menu($c->menu_id);
                echo $pmenu->name;
                ?></td>

              <td class="order">
                <div class="id-col">
                    <input class="id" type="hidden" value="<?php echo $c->id; ?>">
                </div>
                <div class="order-col"><?php echo $c->display_order; ?></div>
              </td>
              <td><?php echo ($c->active == 1) ? 'Yes' : 'No'; ?></td>
              <td><?php echo $c->id; ?></td>
              <td>
                <form method="post" action="#">
                  <input type="hidden" name="wpresto_delete_cat[menu_id]" value="<?php echo $c->menu_id;?>" />
                  <input type="hidden" name="wpresto_delete_cat[category_id]" value="<?php echo $c->id;?>" />
                  <input type="hidden" name="wpresto_delete_cat[name]" value="<?php echo $c->name;?>" />
                  <input type="submit" class="button btn btn-outline-danger" name="wpresto_delete_cat[category]" value="Delete" onclick="return confirm('Are you sure you want to delete this category?')" />
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>

        <div class="WPRESTO-admin-nav">
          <p>
            <a class="button-primary btn btn-primary" href="<?php echo home_url(); ?>/create-category?menu_id=<?php echo $menu_id; ?>">+ Add New Category</a>&nbsp;
            <a class="button btn" href="<?php echo home_url(); ?>/menu-list">&laquo;back to Menus</a>&nbsp;
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    jQuery(document).ready(function ($) {
        $('.table').DataTable({
            "order" : [[4, "desc"]],
            "pageLength": 10,
            "columns" : [
                null,
                null,
                null,
                { "orderable" : false },
                { "width" : "5rem"},
                null,
                null,
                { "orderable" : false }
            ]
        });
        var focused = false;
        jQuery(document).on('click', '.table td .order-col', function(){
            if(!focused){
                focused = true;
                var currentOrder = $(this).text();
                $(this).addClass("editing");
                $(this).html("<input class='new-order' type=\"text\" value=\"" + currentOrder + "\" />" +
                    "<a href=\"#\" class=\"confirm btn btn-sm btn-default btn-primary\">OK</a>"
                );
            }
        });
        jQuery(document).on("click", ".confirm", function (e) {
            e.preventDefault();
            ajax_url = "<?php echo admin_url( 'admin-ajax.php'); ?>";
            var currentValue = $(this).closest("td").find("input.new-order").val();
            var id = $(this).closest("td").find("input.id").val();
            $.ajax({
                url: ajax_url,
                data : {
                    'action' : 'update_category_order',
                    'new_order' : currentValue,
                    'id' : id
                },
                type : 'POST',
                success: function (data) {
                    if(data = "success"){
                        $(document).find(".editing").html(currentValue);
                        $(document).find(".editing").removeClass("editing");
                    }
                    focused = false;
                }
            });
        });
    });
</script>
<?php get_footer() ?>