<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
function header_view_scripts(){
  wp_enqueue_style('materialize-style', "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css");
  wp_enqueue_script("materialize-js", "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js");
  wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', array() );
}
add_action( 'wp_enqueue_scripts', 'header_view_scripts' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php
  $menu_id = isset($_GET['menu_id']) ? $_GET['menu_id'] : 1;
  $inc = new WPRESTO_MENU();
  $temp = $inc->get_by_id($menu_id);
  $lang = 'cn';
  if ($temp['multi_lang'] == "1"){
    $lang = isset($_GET['lang']) ? $_GET['lang'] : 'cn';
  }
  if ($lang == 'en'){
      echo "<title>" . $temp['shop_name'] . "-" . $temp['name'] . "</title>";
  }else{
    echo "<title>" . $temp['shop_name_cn'] . "-" . $temp['name_cn'] . "</title>";
  }
//  var_dump($temp);
  ?>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title><?php echo $title ?></title>
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

