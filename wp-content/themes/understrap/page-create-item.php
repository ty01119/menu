<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */
if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>
    <script> var updated = false;</script>
<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">



      <div class="wrap wpresto col-md-12">
        <h2 class="left">Restaurant Menu Manager - Add Item</h2>
        <div class="clear"></div>
        <hr />

        <?php

        $menu_id = $_GET['menu_id'];

        if(isset($_POST['save_item'])) {
          $newItems = $_POST['wpresto'];
          $item = new WPRESTO_Item();
          if ($_POST['wpresto']['image']=='') $newItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
          $new = $item->create($newItems);
          echo wpresto_alert_msg("Item <b>" . $newItems['name'] . "</b> successfully added! Jump to Item List in 3 Second");

          $uploaddir = WPPRESTO_PATH . "images/";
          $fileName = "food-".$new.".jpg";
          $uploadfile = $uploaddir . $fileName;
          $uploadOk = 0;
          $updatedItems['id'] = $new;
          if (move_uploaded_file($_FILES['item_image']['tmp_name'], $uploadfile)) $uploadOk = 1;
          if($uploadOk==1) $updatedItems['image'] = WPPRESTO_PLUGIN_URL."/images/".$fileName;
          $item->update($updatedItems);
          ?>
          <script>
            updated = true;
          </script>
        <?php }

        $category = new WPRESTO_Category();
        $categories = $category->get_all_by_menu($menu_id);
        $menuLoad = new WPRESTO_Menu($menu_id);

        ?>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                $("#upload_image_file").change(function() {
                    //alert("asd");
                    readURL(this);
                });
                if(updated){
                    setTimeout(function(){
                        window.location.href = "<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu_id; ?>";
                    }, 3000);
                }
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#previewHolder').attr('src', e.target.result);
                        $('#previewHolder').attr('width', 200);

                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>



        <p class="breadcrumb">
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menuLoad->id;?>"><?php echo $menuLoad->name; ?></a>
          <a class="breadcrumb-item" href="">Add Item</a>
        </p>
          <div class="navi sticky-top">
              <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
              <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
              <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
          </div>
          <br>
        <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[name_cn]">名称</label>
                    <input name="wpresto[name_cn]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">中文名称.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[name]">Name</label>
                    <input name="wpresto[name]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">Display name for this item.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description_cn]">描述</label>
                    <textarea name="wpresto[description_cn]" class="large-text code form-control"></textarea>
                    <small class="form-text text-muted">中文描述.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description]">Description</label>
                    <textarea name="wpresto[description]" class="large-text code form-control"></textarea>
                    <small class="form-text text-muted">Display description for this item.</small>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[description]">Upload Image</label>
                    <div class="col-md-12">
                        <div class="row">
                            <input class="form-control col-sm-4" id="upload_image" type="text" size="36" name="wpresto[image]" value="" />
                            <input class="form-control-file col-sm-4" id="upload_image_file" type="file" value="Upload Image" name="item_image"/>
                            <img class="col-sm-4" id="previewHolder" src="#" alt="your image" width=0/>
                        </div>
                    </div>
                    <small class="form-text text-muted">Enter an URL or upload an image for this item.</small>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[price]">Price</label>
                    <input name="wpresto[price]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">Displayed Price. Include denomination if desired.</small>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[second_price]">Secondary Price</label>
                    <input name="wpresto[second_price]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">Use to display half and full order prices, or price with special side etc.</small>
                </div>

                <div class="form-group col-md-6">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="wpresto[show_price]">
                            <input class="form-check-input" type="checkbox" name="wpresto[show_price]" checked value="1"/>Show Price
                        </label>
                    </div>
                    <small class="form-text text-muted">Display the price for this item.</small>
                </div>

                <div class="form-group col-md-6">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="wpresto[show_price]">
                            <input class="form-check-input" type="checkbox" name="wpresto[active]" checked value="1"/>Active
                        </label>
                    </div>
                    <small class="form-text text-muted">Display this item.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[category_id]">Category</label>
                    <select name="wpresto[category_id]" class="regular-text code form-control">
                      <?php foreach($categories as $cat):?>
                          <option value="<?php echo $cat->id;?>" <?php echo ($item->category_id == $cat->id)? 'selected' : '';?>>
                            <?php if ($cat->name == "" && $cat->name_cn != ""){
                                echo $cat->name_cn;
                            }elseif($cat->name != "" && $cat->name_cn == ""){
                              echo $cat->name;
                            }else{
                              echo $cat->name_cn . "&nbsp;/&nbsp;" . $cat->name;
                            }
                            ?>
                          </option>
                      <?php endforeach;?>
                        <option value="0" <?php echo ($item->category_id == "0")? 'selected' : '';?>>None (hidden)</option>
                    </select>
                    <small class="form-text text-muted">Category for this item in this menu. Controls layout of items. Setup Categories and then items.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[display_order]">Display Order</label>
                    <input name="wpresto[display_order]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">Order in which this item is displayed to users.</small>
                </div>
            </div>

          <br />

          <input type="hidden" name="wpresto[menu_id]" value="<?php echo $menu_id?>" />
          <div class="wpresto-admin-nav">
            <p>
              <input class="button-primary btn btn-primary" class="left" type="submit" name="save_item" value="Save Item" />
              <a class="button btn" href="<?php echo home_url(); ?>/edit-menu-item?menu_id=<?php echo $menu_id; ?>">&laquo;back to Items</a>&nbsp;
            </p>
          </div>
        </form>

      </div>



    </div>
  </div>
</div>

<?php get_footer() ?>