<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <div class="wrap wpresto col-md-12">
        <h2 class="left">Restaurant Menu Manager - Create Category</h2>
        <div class="clear"></div>
        <hr />
        <?php

        //$menu = new wpresto_Menu((int) $_GET['menu_id']);
        //$category = new wpresto_Category();
        //$category->menu_id = $menu->id;

        if(isset($_POST['save_category'])) {
          $newcat = $_POST['wpresto'];
          $newcat['menu_id'] = $_POST['menu_id'];
          $cat = new WPRESTO_Category();
          if (!isset($_POST['wpresto']['show_title'])) $newcat['show_title']=0;
          if (!isset($_POST['wpresto']['show_description'])) $newcat['show_description']=0;
          if (!isset($_POST['wpresto']['active'])) $newcat['active']=0;
          $new = $cat->create($newcat);
          echo wpresto_alert_msg("Category <b>" . $newcat['name'] . "</b> successfully added! ");

        }

        $menuList = new WPRESTO_MENU();
        if (isset($_GET['menu_id'])){
          $menus = $menuList->get_all(intval($_GET['menu_id']));
        }else{
          $menus = $menuList->get_all();

        }

        ?>

        <p class="breadcrumb">
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/category-list">Categories</a>
          <a class="breadcrumb-item" href="">Add Category</a>
        </p>

          <div class="navi sticky-top">
              <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $_GET['menu_id']; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
              <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $_GET['menu_id']; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
              <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $_GET['menu_id']; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
          </div>
          <br>
        <form method="POST" action="#">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[name_cn]">名称</label>
                    <input name="wpresto[name_cn]" type="text" value="" class="regular-text form-control">
                    <small class="form-text text-muted">中文名称.</small>
                </div>
                <div class="form-group col-md-6">
                  <label for="wpresto[name]">Name</label>
                  <input name="wpresto[name]" type="text" value="" class="regular-text form-control">
                  <small class="form-text text-muted">Display name for this category.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description_cn]">描述</label>
                    <textarea name="wpresto[description_cn]" class="large-text code form-control"></textarea>
                    <small class="form-text text-muted">中文描述.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description]">Description</label>
                    <textarea name="wpresto[description]" class="large-text code form-control"></textarea>
                    <small class="form-text text-muted">Display description for this category.</small>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[menu_id]">Link to Menu</label>
                    <select name="menu_id" class="regular-text code form-control">
                      <?php foreach($menus as $m):?>
                        <?php if($m->id == $_GET['menu_id']){ ?>
                          <option value="<?php echo $m->id;?>" selected><?php echo $m->name;?></option>
                        <?php }else{ ?>
                          <option value="<?php echo $m->id;?>"><?php echo $m->name;?></option>
                        <?php } ?>
                      <?php endforeach;?>
                        <option value="0">None (hidden)</option>
                    </select>
                    <small class="form-text text-muted">Link this category to a menu.</small>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check form-check-inline">
                        <label for="wpresto[show_title]" class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="wpresto[show_title]" value="1" <?php echo ($category->show_title == 1) ? 'checked' : '';?>/>
                            Show Title?
                        </label>
                        <small class="form-text text-muted">Display title text on frontend for category.</small>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check form-check-inline">
                        <label for="wpresto[show_title]" class="form-check-label">
                            <input type="hidden" name="wpresto[show_description]" value="0" />
                            <input type="checkbox" class="form-check-input" name="wpresto[show_description]" value="1" <?php echo ($category->show_description == 1) ? 'checked' : '';?>/>
                            Show Description?
                        </label>
                        <small class="form-text text-muted">Display description text on frontend for category.</small>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check form-check-inline">
                        <label for="wpresto[show_title]" class="form-check-label">
                            <input type="hidden" name="wpresto[active]" value="0" />
                            <input type="checkbox" class="form-check-input" name="wpresto[active]" value="1" checked/>
                            Active ?
                        </label>
                        <small class="form-text text-muted">Display items in this category.</small>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[display_order]">Display Order</label>
                    <?php $display_order = empty($category->display_order) ? "0" : $category->display_order; ?>
                    <input name="wpresto[display_order]" type="text" value="<?php echo $display_order;?>" class="regular-text form-control">
                    <small class="form-text text-muted">Order in which this category is displayed to users.</small>
                </div>

                <div class="col-md-12">
                    <input type="hidden" name="wpresto_crud[category]" value="new" />

                    <div class="wpresto-admin-nav">
                        <p>
                            <input class="button-primary left btn btn-primary" type="submit" name="save_category" value="Save Category" />
                        </p>
                    </div>
                </div>

            </div>

        </form>

      </div>


    </div>
  </div>
</div>

<?php get_footer() ?>