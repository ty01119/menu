<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <div class="wrap WPPRESTO col-md-12">
        <h2 class="left">Restaurant Menu Manager - Setup Menus</h2>
        <div class="clear"></div>
        <hr />
        <?php

        if(isset($_POST['wpresto_delete']['menu']))
        {
          $del_menu_id = $_POST['wpresto_delete']['menu_id'];
          $menu1 = new WPRESTO_Menu();
          $menu1->destroy($del_menu_id);
          echo wpresto_alert_msg("Menu <b>" . $_POST['wpresto_delete']['menu_name'] . "</b> successfully deleted! ");

        }


        $menuList = new WPRESTO_MENU();
        $menus = $menuList->get_all();

        ?>


        <table class="widefat table table-striped">
          <thead>
          <tr>
            <th>名称</th>
            <th>Name</th>
            <th>Edit Menu</th>
            <th>Edit Menu Category Set</th>
            <th>Edit Menu Items</th>
            <th>Delete</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach($menus as $m): ?>
            <tr>
              <td><strong><?php echo $m->name_cn; ?></strong></td>
              <td><strong><?php echo $m->name; ?></strong></td>
              <td><a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $m->id; ?>"><?php echo $m->name; ?></a></td>

              <td><a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $m->id; ?>">Edit Categories (<?php echo getTotalCategory($m->id);?>)</a></td>
              <td><a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $m->id; ?>">Edit Items (<?php echo getTotalItems($m->id);?>)</a></td>
              <td>
                <form method="post" action="#">
                  <input type="submit" class="button btn btn-outline-danger" name="wpresto_delete[menu]" value="Delete" onclick="return confirm('Are you sure you want to delete [<?php echo $m->name; ?>] ?.')" />
                  <input type="hidden" name="wpresto_delete[menu_id]" value="<?php echo $m->id;?>" />
                  <input type="hidden" name="wpresto_delete[menu_name]" value="<?php echo $m->name;?>" />
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>



    </div>
  </div>
</div>
<?php
get_footer();
?>