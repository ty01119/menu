<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">



      <div class="wrap WPRESTO col-md-12">
        <h2 class="left">Restaurant Menu Manager - Setup Items</h2>
        <div class="clear"></div>
        <hr />

        <?php

        if(isset($_POST['wpresto_delete']['item']))
        {
          $del_menu_id = $_POST['wpresto_delete']['menu_id'];
          $del_item_id = $_POST['wpresto_delete']['item_id'];
          $item3 = new WPRESTO_Item();
          $item3->destroySingle($del_item_id, $del_menu_id);
          echo wpresto_alert_msg("Item <b>" . $_POST['wpresto_delete']['item_name'] . "</b> successfully deleted! ");

        }

        $menu_id = $_GET['menu_id'];
        $menu = new WPRESTO_Menu($menu_id);
        $item = new WPRESTO_Item();
        $items = $item->get_all($menu->id);
        //$category = new WPRESTO_Category();
        //$categories = $category->get_all($item->category_id);

        if($_GET['sort']==1) {
          global $wpdb;
          foreach ($_GET['item-list'] as $position => $item)
          {
            if($item!='') {
              $sql = "UPDATE " .WPPRESTO_ITEM_DB ." SET display_order = $position WHERE id = $item AND menu_id = $menu_id";
              $wpdb->query($sql);
            }
          }
        }

        ?>

        <style>
          .myDragClass {
            background-color:orange;
          }

          .hover {
            /*/background-color:orange;*/
          }

        </style>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                //<span class="comment"></span>
                $("#item-list").DataTable({
                    "order" : [[6, "desc"]],
                    "pageLength": 10,
                    "columns" : [
                        null,
                        null,
                        { "orderable" : false },
                        null,
                        null,
                        { "orderable" : false },
                        { "width" : "5rem"},
                        null,
                        { "orderable" : false },
                        { "orderable" : false },
                    ],
                });

                var focused = false;
                jQuery(document).on('click', '.table td .order-col', function(){
                    if(!focused){
                        focused = true;
                        var currentOrder = $(this).text();
                        $(this).addClass("editing");
                        $(this).html("<input class='new-order' type=\"text\" value=\"" + currentOrder + "\" />" +
                            "<a href=\"#\" class=\"confirm btn btn-sm btn-default btn-primary\">OK</a>"
                        );
                    }
                });
                jQuery(document).on("click", ".confirm", function (e) {
                    e.preventDefault();
                    ajax_url = "<?php echo admin_url( 'admin-ajax.php'); ?>";
                    var currentValue = $(this).closest("td").find("input.new-order").val();
                    var id = $(this).closest("td").find("input.id").val();
                    $.ajax({
                        url: ajax_url,
                        data : {
                            'action' : 'update_item_order',
                            'new_order' : currentValue,
                            'id' : id
                        },
                        type : 'POST',
                        success: function (data) {
                            if(data = "success"){
                                $(document).find(".editing").html(currentValue);
                                $(document).find(".editing").removeClass("editing");
                            }
                            focused = false;
                        }
                    });
                });
            });
        </script>



        <p class="wpresto-breadcrumb breadcrumb">
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
          <a class="breadcrumb-item" href=""><?php echo $menu->name; ?></a>

        </p>

          <div class="navi sticky-top">
              <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
              <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
              <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu_id; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
          </div>
          <br>
        <table class="widefat table table-striped" id="item-list">
          <thead>
          <tr>
            <th>名称</th>
            <th>Name</th>
            <th>Parent Menu</th>
            <th>Category</th>
            <th>Price</th>
            <th>Image</th>
            <th>Order</th>
            <th>Active</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach($items as $i): ?>
            <tr id="<?php echo $i->id;?>" style="border-bottom: 1px solid">
              <td><strong><?php echo $i->name_cn; ?></strong></td>
              <td><strong><?php echo $i->name; ?></strong></td>
              <td><?php echo $menu->name;?></td>
              <td>
                <a href="<?php echo home_url(); ?>/edit-category?cat_id=<?php echo $i->category_id;?>&menu_id=<?php echo $menu_id; ?>">
                  <?php
                  $category = new WPRESTO_Category();
                  $cat = $category->get_all($i->category_id);
                  if ($cat[0]->name == "" && $cat[0]->name_cn != ""){
                    echo $cat[0]->name_cn;
                  }elseif ($cat[0]->name != "" && $cat[0]->name_cn == ""){
                    echo $cat[0]->name;
                  }else{
                    echo $cat[0]->name_cn . "&nbsp;/&nbsp;" . $cat[0]->name;
                  }
                  unset($category);
                  ?>
                </a>
              </td>
              <td><?php echo $i->price;?></td>
              <td>
                <?php if(!empty($i->image)):?>
                  <img class="WPRESTO_preview_item_image" src="<?php echo $i->image;?>" width="40"/>
                <?php endif;?>
              </td>
                <td class="order">
                    <div class="id-col">
                        <input class="id" type="hidden" value="<?php echo $i->id; ?>">
                    </div>
                    <div class="order-col"><?php echo $i->display_order; ?></div>
                </td>
              <td><?php echo ($i->active == 1) ? 'Yes' : 'No'; ?></td>
              <td><a href="<?php echo home_url(); ?>/edit-item?item_id=<?php echo $i->id; ?>">Edit Item</a></td>
              <td>
                <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                  <input type="hidden" name="wpresto_delete[menu_id]" value="<?php echo $i->menu_id;?>" />
                  <input type="hidden" name="wpresto_delete[item_name]" value="<?php echo $i->name;?>" />
                  <input type="hidden" name="wpresto_delete[item_id]" value="<?php echo $i->id;?>" />
                  <input type="submit" class="button btn btn-outline-danger" name="wpresto_delete[item]" value="Delete" onclick="return confirm('Are you sure you want to delete <?php echo $i->name; ?> item?')"/>
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>

        <div class="WPRESTO-admin-nav">
          <p>
            <a class="button-primary btn btn-primary" href="<?php echo home_url(); ?>/create-item?menu_id=<?php echo $menu_id ?>">+ Add New Item</a>&nbsp;
            <a class="button btn" href="<?php echo home_url(); ?>/menu-list">&laquo;back</a>&nbsp;
          </p>
        </div>
      </div>



    </div>
  </div>
</div>

<?php get_footer() ?>