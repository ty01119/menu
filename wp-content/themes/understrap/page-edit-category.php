<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <div class="wrap WPRESTO col-md-12">
        <h2 class="left">Restaurant Menu Manager - Edit Menu Category</h2>
        <div class="clear"></div>
        <hr />
        <?php
        if(isset($_POST['save_category'])) {
          $updatedCat = $_POST['wpresto'];
          $updatedCat['menu_id'] = $_POST['menu_id'];
          if (!isset($_POST['wpresto']['show_title'])) $updatedCat['show_title']=0;
          if (!isset($_POST['wpresto']['show_description'])) $updatedCat['show_description']=0;
          if (!isset($_POST['wpresto']['active'])) $updatedCat['active']=0;
          $category = new WPRESTO_Category();
          $cat = $category->update($updatedCat);
          //print_r($_POST);
          //print_r($updatedCat);
          echo wpresto_alert_msg("Category <b>" . $updatedCat['name'] . "</b> successfully updated! ");
        }

        $cat_id = $_GET['cat_id'];
        $category = new WPRESTO_Category((int) $_GET['cat_id']);

        $menuList = new WPRESTO_MENU();
        $menus = $menuList->get_all();
        $currentMenu = new WPRESTO_MENU();
        $menu = $currentMenu->get_by_id($category->menu_id);


        ?>


        <p class="wpresto-breadcrumb breadcrumb">
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu['id']; ?>">Categories</a>
          <a class="breadcrumb-item" href="">Edit <?php echo $category->name;?></a>

        </p>
        <div class="navi sticky-top">
          <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu['id']; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
          <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu['id']; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
          <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu['id']; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
        </div>
        <br>
        <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[name_cn]">名称</label>
                    <input name="wpresto[name_cn]" type="text" value="<?php echo $category->name_cn;?>" class="regular-text form-control">
                    <small class="form-text text-muted">中文名称.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[name]">Name</label>
                    <input name="wpresto[name]" type="text" value="<?php echo $category->name;?>" class="regular-text form-control">
                    <small class="form-text text-muted">Display name for this category.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description_cn]">描述</label>
                    <textarea name="wpresto[description_cn]" class="large-text code form-control"><?php echo $category->description_cn;?></textarea>
                    <small class="form-text text-muted">中文描述.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description]">Description</label>
                    <textarea name="wpresto[description]" class="large-text code form-control"><?php echo $category->description;?></textarea>
                    <small class="form-text text-muted">Display description for this category.</small>
                </div>

                <div class="form-group col-md-12">
                    <label for="wpresto[menu_id]">Link to Menu</label>
                    <select name="menu_id" class="regular-text code form-control">
                      <?php foreach($menus as $m):?>
                          <option value="<?php echo $m->id;?>" <?php echo ($category->menu_id==$m->id) ? "selected" : "" ;?>><?php echo $m->name;?></option>
                      <?php endforeach;?>
                        <option value="0" <?php echo ($category->menu_id==0) ? "selected" : "" ;?>>None (hidden)</option>
                    </select>
                    <small class="form-text text-muted">Link this category to a menu</small>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check-inline">
                        <label class="form-check-label" for="wpresto[show_title]">
                            <input class="form-check-input" type="checkbox" name="wpresto[show_title]" value="1" <?php echo ($category->show_title == 1) ? 'checked' : '';?>/>
                            Show Title?
                        </label>
                        <small class="form-text text-muted">Display title text on frontend for category.</small>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check-inline">
                        <label class="form-check-label" for="wpresto[show_description]">
                            <input class="form-check-input" type="checkbox" name="wpresto[show_description]" value="1" <?php echo ($category->show_description == 1) ? 'checked' : '';?>/>
                            Show Description?
                        </label>
                        <small class="form-text text-muted">Display description text on frontend for category.</small>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="form-check-inline">
                        <label class="form-check-label" for="wpresto[active]">
                            <input class="form-check-input" type="checkbox" name="wpresto[active]" value="1" <?php echo ($category->active == 1) ? 'checked' : '';?>/>
                            Active
                        </label>
                        <small class="form-text text-muted">Display items in this category.</small>
                    </div>
                </div>

                <div class="from-group col-md-12">
                    <label for="wpresto[display_order]">Display Order</label>
                    <input name="wpresto[display_order]" type="text" value="<?php echo $category->display_order;?>" class="regular-text form-control">
                    <small class="form-text text-muted">Order in which this category is displayed to users.</small>
                </div>
            </div>

          <br />

          <input type="hidden" name="wpresto[id]" value="<?php echo $category->id;?>" />

          <div class="WPRESTO-admin-nav">
            <p>
              <input class="button-primary btn btn-primary" class="left" type="submit" name="save_category" value="Save Category" />&nbsp;
              <!--a class="button" href="admin.php?page=main_menu">&laquo;back</a-->&nbsp;
            </p>
          </div>


        </form>

      </div>

    </div>
  </div>
</div>

<?php get_footer() ?>