<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>
    <script>
        var updated = false;
    </script>
<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">


        <div class="wrap wpresto col-md-12">
            <h2 class="left">Restaurant Menu Manager - Edit Items</h2>
            <div class="clear"></div>
            <hr />


          <?php

          $item_id = isset($_GET['item_id']) ? $_GET['item_id'] : '';
          $menu_id = isset($_GET['menu_id']) ? $_GET['menu_id'] : '';

          if(isset($_POST['update_item'])) {
            //die("iamge: " . $_POST['item_image']);
            //print_r($_FILES);//['wpresto']['item_image']);
            $updatedItems = $_POST['wpresto'];
            $updatedItems['menu_id'] = $_POST['menu_id'];

            if (!isset($_POST['wpresto']['active'])) $updatedItems['active']=0;
            if (!isset($_POST['wpresto']['show_price'])) $updatedItems['show_price']=0;

            $currentImg = $_POST['current_item_image'];

            $uploaddir = WPPRESTO_PATH . "images/";
            $name = $_FILES['item_image']['name'];
            $ext = end((explode(".", $name)));
            $fileName = "food-".$item_id.".".$ext;
            $uploadfile = $uploaddir . $fileName;
            $uploadOk = 0;

            if (move_uploaded_file($_FILES['item_image']['tmp_name'], $uploadfile)) {
              $uploadOk = 1;
            } else {
              //echo wpresto_alert_msg("There was an error in uploading");;
            }
            if($_FILES['item_image']['name']!='')
            {
              if($uploadOk==1) {
                $updatedItems['image'] = WPPRESTO_PLUGIN_URL."/images/".$fileName;
              } else {
                if ($_POST['wpresto']['image']=='') $updatedItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
              }
            } else {
              $updatedItems['image'] = $currentImg;
            }
            //if ($updatedItems['image']=='') $updatedItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
            $item2 = new wpresto_Item();
            $new = $item2->update($updatedItems);
            echo wpresto_alert_msg("Item <b>" . $_POST['wpresto']['name'] . "</b> successfully updated! Jump to Item List in 3 Second");
            //print_r($updatedItems);
            //print_r($_FILES);
            ?>
            <script>
             updated = true;
            </script>
          <?php }

          $item = new wpresto_Item((int) $_GET['item_id']);
          $menu = new wpresto_Menu((int) $item->menu_id);

          $category = new wpresto_Category();
          $categories = $category->get_all_by_menu($item->menu_id);

          $menuList = new WPRESTO_MENU();
          $menus = $menuList->get_all();

          ?>

            <script type="text/javascript">
                jQuery(document).ready(function($){
                    jQuery("#upload_image_file").change(function($) {
                        readURL(this);
                    });
                    if(updated){
                        setTimeout(function(){
                            window.location.href = "<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $item->menu_id; ?>";
                        }, 3000);
                    }
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#previewHolder').attr('src', e.target.result);
                            $('#previewHolder').attr('width', 200);

                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }

            </script>

            <p class="wpresto-breadcrumb breadcrumb">
                <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
              <?php if($_GET['referrer']=='fooditems') { ?>
                  <a class="breadcrumb-item" href="<?php echo home_url(); ?>/item-list">Item List</a>
              <?php } else { ?>
                  <a class="breadcrumb-item" href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu->id;?>"><?php echo $menu->name; ?></a>
                  <a class="breadcrumb-item" href="<?php echo home_url(); ?>/edit-item?item_id=<?php echo $item_id;?>"><?php echo $item->name_cn; ?></a>
              <?php } ?>
                <a class="breadcrumb-item" href="">Edit Item</a>
            </p>

            <div class="navi sticky-top">
                <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
                <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
                <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
            </div>
            <br>
            <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="wpresto[name_cn]">名称</label>
                        <input name="wpresto[name_cn]" type="text" value="<?php echo $item->name_cn;?>" class="regular-text form-control">
                        <small class="form-text text-muted">中文名称.</small>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="wpresto[name]">Name</label>
                        <input name="wpresto[name]" type="text" value="<?php echo $item->name;?>" class="regular-text form-control">
                        <small class="form-text text-muted">Display name for this item.</small>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="wpresto[description_cn]">描述</label>
                        <textarea name="wpresto[description_cn]" class="large-text code form-control"><?php echo $item->description_cn;?></textarea>
                        <small class="form-text text-muted">中文描述.</small>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="wpresto[description]">Description</label>
                        <textarea name="wpresto[description]" class="large-text code form-control"><?php echo $item->description;?></textarea>
                        <small class="form-text text-muted">Display description for this item.</small>
                    </div>

                </div>

                <div class="form-group">
                    <label for="wpresto[menu_id]">Link to Menu</label>
                    <select name="menu_id" class="regular-text code form-control">
                      <?php foreach($menus as $m):?>
                          <option value="<?php echo $m->id;?>" <?php echo ($item->menu_id == $m->id)? 'selected' : '';?>><?php echo $m->name;?></option>
                      <?php endforeach;?>
                        <option value="0" <?php echo ($item->menu_id == "0")? 'selected' : '';?>>None (hidden)</option>
                    </select>
                    <small class="form-text text-muted">Link this item to a menu</small>
                </div>

                <div class="form-group">
                    <label for="upload_image">
                        Upload Image or Input Image URL
                    </label>
                    <div class="row">
                        <div class="col-md-4">
                            <input class="form-control" id="upload_image" type="text" size="36" name="wpresto[image]" value="" />
                            <span class="description">Enter an URL or upload an image for this item.</span><br>
                        </div>
                        <div class="col-md-4">
                            <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
                            <input id="current_image_file" type="hidden" value="<?php echo $item->image; ?>" name="current_item_image"/>
                            <input class="form-control-file" id="upload_image_file" type="file" value="Upload Image" name="item_image"/>
                            <span class="description">Upload an image for this item.</span><br>
                        </div>
                        <div class="col-md-4">
                            <img id="previewHolder" src="<?php echo $item->image; ?>" alt="your image" width=200/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="wpresto[price]">Price</label>
                        <input name="wpresto[price]" type="text" value="<?php echo $item->price;?>" class="regular-text form-control">
                        <small class="form-text text-muted">Displayed Price. Include denomination if desired.</small>
                    </div>

                    <div class="form-group">
                        <label for="wpresto[second_price]">Secondary Price</label>
                        <input name="wpresto[second_price]" type="text" value="<?php echo $item->second_price;?>" class="regular-text form-control">
                        <small class="form-text text-muted">Use to display half and full order prices, or price with special side etc.</small>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label" for="wpresto[show_price]">
                                        <input class="form-check-input" type="checkbox" name="wpresto[show_price]" value="1" <?php echo ($item->show_price == 1) ? 'checked' : '';?>/>
                                        Show Price
                                    </label>
                                    <small class="form-text text-muted">Display the price for this item.</small>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label" for="wpresto[active]">
                                        <input class="form-check-input" type="checkbox" name="wpresto[active]" value="1" <?php echo ($item->active == 1) ? 'checked' : '';?>/>
                                        Active
                                    </label>
                                    <small class="form-text text-muted">Display this item.</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="wpresto[category_id]">Category</label>
                                <select name="wpresto[category_id]" class="regular-text code form-control">
                                  <?php foreach($categories as $cat):?>
                                      <option value="<?php echo $cat->id;?>" <?php echo ($item->category_id == $cat->id)? 'selected' : '';?>>
                                        <?php if($cat->name =='' && $cat->name_cn != "") {
                                          echo $cat->name_cn;
                                        }elseif($cat->name !='' && $cat->name_cn == ""){
                                          echo $cat->name;
                                        }else{
                                          echo $cat->name_cn . "&nbsp;/&nbsp;" . $cat->name;
                                        }
                                        ?>
                                      </option>
                                  <?php endforeach;?>
                                    <option value="0" <?php echo ($item->category_id == "0")? 'selected' : '';?>>None (hidden)</option>
                                </select>
                                <small class="form-text text-muted">Category for this item in this menu.</small>
                            </div>
                            <div class="col-md-6">
                                <label for="wpresto[display_order]">Display Order</label>
                                <input name="wpresto[display_order]" type="text" value="<?php echo $item->display_order;?>" class="regular-text form-control">
                                <small class="form-text text-muted">Order in which this item is displayed to users.</small>
                            </div>
                        </div>
                    </div>

                </div>

                <br />

                <input type="hidden" name="wpresto[id]" value="<?php echo $item->id;?>" />

                <div class="wpresto-admin-nav">
                    <p>
                        <input class="button-primary btn btn-primary" class="left" type="submit" name="update_item" value="Save Item" />
                        <a class="button btn btn-success" href="<?php echo home_url(); ?>/create-item?menu_id=<?php echo $item->menu_id; ?>">+ Add New Item</a>&nbsp;
                        <a class="button btn" href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $item->menu_id; ?>">&laquo;back to Items</a>&nbsp;
                    </p>
                </div>


            </form>

        </div>




    </div>
  </div>
</div>

<?php get_footer() ?>