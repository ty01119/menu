<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */
if( !( (current_user_can('editor') || current_user_can('administrator') || current_user_can('admin')) && is_user_logged_in()) ){
  wp_logout();
  wp_redirect(site_url('/login'));
}
get_header('control');

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <div class="wrap wpresto col-md-12">
        <h2 class="left">Restaurant Menu Manager - Edit Menu</h2>
        <div class="clear"></div>
        <hr />

        <?php

        $item_id = $_GET['item_id'];
        $menu_id = $_GET['menu_id'];

        if(isset($_POST['update_menu'])) {
          $updatedMenu = $_POST['wpresto'];
          if (!isset($_POST['wpresto']['show_title'])) $updatedMenu['show_title']=0;
          if (!isset($_POST['wpresto']['show_link'])) $updatedMenu['show_link']=0;
          $menu2 = new WPRESTO_Menu();
          $new = $menu2->update($updatedMenu);
          echo wpresto_alert_msg("Menu <b>" . $updatedMenu['name'] . "</b> successfully updated! ");

        }

        //wpresto_save_menu();
        $menu = new WPRESTO_Menu((int) $_GET['menu_id']);

        ?>



        <p class="wpresto-breadcrumb breadcrumb">
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/menu-list">Menus</a>
          <a class="breadcrumb-item" href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu->id;?>"><?php echo $menu->name; ?></a>
        </p>
        <div class="navi sticky-top">
            <a href="<?php echo home_url(); ?>/edit-menu?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-primary" role="button" aria-pressed="true">EDIT SHOP INFO</a>
            <a href="<?php echo home_url(); ?>/category-list?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-success" role="button" aria-pressed="true">LIST CATEGORIES</a>
            <a href="<?php echo home_url(); ?>/edit-menu-items?menu_id=<?php echo $menu->id; ?>" class="btn btn-outline-info" role="button" aria-pressed="true">LIST ITEMS</a>
        </div>
        <br>
        <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[name_cn]">菜单名称</label>
                    <input name="wpresto[name_cn]" type="text" value="<?php echo $menu->name_cn;?>" class="regular-text form-control">
                    <small class="form-text text-muted">中文名称.</small>
                </div>
                <div class="form-group col-md-6">
                    <label for="wpresto[name]">Name</label>
                    <input name="wpresto[name]" type="text" value="<?php echo $menu->name;?>" class="regular-text form-control">
                    <small class="form-text text-muted">Display name for this menu.</small>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[description_cn]">描述</label>
                    <textarea name="wpresto[description_cn]" class="large-text code form-control"><?php echo $menu->description_cn;?></textarea>
                    <small class="form-text text-muted">中文描述.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[description]">Description</label>
                    <textarea name="wpresto[description]" class="large-text code form-control"><?php echo $menu->description;?></textarea>
                    <small class="form-text text-muted">Display description for this menu.</small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="wpresto[shop_name_cn]">品牌名称</label>
                    <textarea name="wpresto[shop_name_cn]" class="large-text code form-control"><?php echo $menu->shop_name_cn;?></textarea>
                    <small class="form-text text-muted">品牌名称.</small>
                </div>

                <div class="form-group col-md-6">
                    <label for="wpresto[shop_name]">Brand Name</label>
                    <textarea name="wpresto[shop_name]" class="large-text code form-control"><?php echo $menu->shop_name;?></textarea>
                    <small class="form-text text-muted">Display Brand Name.</small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="wpresto[address]">Address</label>
                    <input name="wpresto[address]" class="large-text code form-control" value="<?php echo $menu->address;?>">
                    <small class="form-text text-muted">Enter Address Here.</small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="wpresto[phone]">Phone</label>
                    <input name="wpresto[phone]" class="large-text code form-control" value="<?php echo $menu->phone;?>">
                    <small class="form-text text-muted">Enter Phone Number Here.</small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="wpresto[open_time]">Open Time</label>
                    <input name="wpresto[open_time]" class="large-text code form-control" value="<?php echo $menu->open_time;?>">
                    <small class="form-text text-muted">Enter Opening Time Here.</small>
                </div>
            </div>

            <div class="form-group">
                <div class="form-check">
                    <label class="form-check-label" for="wpresto[show_title]">
                        <input class="form-check-input" type="checkbox" name="wpresto[show_title]" value="1" <?php echo ($menu->show_title == 1) ? 'checked' : '';?>/>
                        Show title on frontend?
                    </label>
                    <small class="form-text text-muted">Display menu title?</small>
                </div>
            </div>

            <input type="hidden" name="wpresto[id]" value="<?php echo $menu->id;?>" />

            <div class="wpresto-admin-nav">
            <p>
              <input class="button-primary btn btn-primary" class="left" type="submit" name="update_menu" value="Save Menu" />&nbsp;
              <a class="button btn" href="<?php echo home_url(); ?>/menu-list">&laquo;back</a>&nbsp;

            </p>
            </div>

        </form>

      </div>

    </div>
  </div>
</div>

<?php get_footer() ?>
