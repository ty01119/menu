<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

get_header('view');

$menuId = isset($_GET['menu_id']) ? $_GET['menu_id'] : 1;
$menuInstance = new WPRESTO_MENU();
$menu = $menuInstance->get_by_id($menuId);

$categoryInstance = new WPRESTO_Category();
$categories = $categoryInstance->get_all_actived_by_menu($menuId);
$lang = 'cn';
if ($menu['multi_lang'] == "1"){
  $lang = isset($_GET['lang']) ? $_GET['lang'] : $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);;
}

?>
<div class="front-view">
<div class="fixed header">
    <div class="wrap">
        <div class="logo">
            <?php if($lang == "en"){ ?>
            <div><?php echo $menu['shop_name']; ?> - <?php echo $menu['name']; ?></div>
            <?php }else{ ?>
            <div><?php echo $menu['shop_name_cn']; ?> - <?php echo $menu['name_cn']; ?></div>
            <?php } ?>
        </div>
      <?php if($menu['multi_lang'] == 1){ ?>
        <div class="language">
          <?php if($lang == "en"){ ?>
            <a class="waves-effect waves-red btn-flat" href="<?php echo site_url(); ?>/menu-view/?lang=cn&menu_id=<?php echo $menuId; ?>">中文</a>&nbsp;&nbsp;
          <?php }else{ ?>
            <a class="waves-effect waves-red btn-flat" href="<?php echo site_url(); ?>/menu-view/?lang=en&menu_id=<?php echo $menuId; ?>">EN</a>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
    <div class="navi">
        <div class="wrap">
            <ul class="section table-of-contents">
              <?php foreach ($categories as $category): ?>
                <?php
                $itemInstance = new WPRESTO_Item();
                $items = $itemInstance->get_all_actived_by_cat($category->id, $menuId);
                ?>
                <?php if(sizeof($items)): ?>
                      <li>
                          <a href="#<?php echo "cat-" . str_replace(" ", "-", $category->id); ?>">
                            <?php if($lang == "en"){
                              echo $category->name;
                            }else{
                              echo $category->name_cn;
                            } ?>
                          </a>
                      </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>
<div class="row no-gutters outer">
  <div class="col s12">
      <div class="wrap">
        <?php foreach ($categories as $category): ?>
        <?php
        $itemInstance = new WPRESTO_Item();
        $items = $itemInstance->get_all_actived_by_cat($category->id, $menuId);
        ?>
          <?php if(sizeof($items)): ?>
            <div id="<?php echo "cat-" . str_replace(" ", "-", $category->id); ?>" class="section scrollspy">
                <div class="category">
                  <?php if($lang == "en"){
                      echo $category->name;
                  }else{
                      echo $category->name_cn;
                  } ?>
                </div>
              <?php foreach ( $items as $item ) {
                if($lang == "en" && $item->name ==""){
                    continue;
                }else{
              ?>
                  <div class="item row no-gutters">
                      <div class="image col s4">
                          <img src="<?php echo $item->image; ?>">
                      </div>
                      <div class="info col s8">
                          <div class="title">
                            <?php if($lang == "en"){
                              echo $item->name;
                            }else{
                              echo $item->name_cn;
                            } ?>
                          </div>
                          <?php if ($item->show_price && $item->second_price){ ?>
                            <div class="price">
                                <?php
                                $price = (number_format((float)$item->price, 2, '.', ',') == 0) ? $item->price : number_format((float)$item->price, 2, '.', ',');
                                $second_price = (number_format((float)$item->second_price, 2, '.', ',') == 0) ? $item->second_price : number_format((float)$item->second_price, 2, '.', ',');
                                ?>
                              <span class="cross">$&nbsp;<?php echo $price; ?></span>&nbsp;&nbsp;
                              <span class="text-red">$&nbsp;<?php echo $second_price; ?></span>
                            </div>
                          <?php }elseif($item->show_price && !$item->second_price){ ?>
                            <?php $price = (number_format((float)$item->price, 2, '.', ',') == 0) ? $item->price : number_format((float)$item->price, 2, '.', ','); ?>
                              <div class="price">
                                  <span class="text-red">$&nbsp;<?php echo $price; ?></span>
                              </div>
                          <?php } ?>

                          <div class="description">
                            <?php if($lang == "en"){
                              echo $item->description;
                            }else{
                              echo $item->description_cn;
                            } ?>
                          </div>
                      </div>
                  </div>
              <?php }} ?>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
  </div>
</div>
<div class="menu-bottom col s12 fixed">
    <div class="row no-gutters text-center">
      <?php if($lang == "en"){ ?>
        <div class="shop-info col s6">
            <a href="<?php echo site_url(); ?>/shop-info?menu_id=<?php echo $menuId; ?>&lang=en">SHOP INFO</a>
        </div>
        <div class="shops col s6">
            SHOPS
            <div class="menu-dropup col s6">
                <ul>
                  <?php
                  $allMenuInstance = new WPRESTO_MENU();
                  $menus = $allMenuInstance->get_all();
                  foreach ($menus as $singleMenu){ ?>
                      <li>
                          <a href="<?php echo site_url(); ?>/menu-view?lang=en&menu_id=<?php echo $singleMenu->id; ?>">
                            <?php echo $singleMenu->name; ?>
                          </a>
                      </li>
                  <?php } ?>
                </ul>
            </div>
        </div>
      <?php }else{ ?>
          <div class="shop-info col s6">
              <a href="<?php echo site_url(); ?>/shop-info?lang=cn&menu_id=<?php echo $menuId; ?>">店铺信息</a>
          </div>
          <div class="shops col s6">
              所有店铺
          </div>
          <div class="menu-dropup col s6">
              <ul>
                <?php
                $allMenuInstance = new WPRESTO_MENU();
                $menus = $allMenuInstance->get_all();
                foreach ($menus as $singleMenu){ ?>
                    <li>
                        <a href="<?php echo site_url(); ?>/menu-view?lang=cn&menu_id=<?php echo $singleMenu->id; ?>">
                          <?php echo $singleMenu->name_cn; ?>
                        </a>
                    </li>
                <?php } ?>
              </ul>
          </div>
      <?php } ?>
    </div>
</div>
</div>
<script>
jQuery(document).ready(function($){
    $('.scrollspy').scrollSpy({
        getActiveElement : function(id){
            var outerContent = $('.table-of-contents');
            var innerContent = $('.table-of-contents a[href="#' + id + '"]');
            outerContent.scrollLeft((innerContent.offset().left + innerContent.width() / 2) - outerContent.width() / 2);
            return 'a[href="#' + id + '"]';
        },
        scrollOffset : 110
    });
    $('.item .image img').click(function(){
       var url = $(this).attr('src');
       console.log(url);
       $('.modal-content').html('<img src="' + url + '">');
        $('.modal').fadeIn();
    });
    $('.modal-close').click(function(e){
        e.preventDefault();
        $('.modal').fadeOut();
    });
    $('.shops').click(function(e){
        if ( $( ".menu-dropup" ).is( ":hidden" ) ) {
            $( ".menu-dropup" ).slideDown();
        } else {
            $( ".menu-dropup" ).slideUp();
        }
    });
});
</script>
<?php
get_footer('view');
?>
