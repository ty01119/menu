<?php
/**
 * Created by PhpStorm.
 * User: bill
 * Date: 10/10/17
 * Time: 4:18 PM
 */

get_header('view');

$menuId = isset($_GET['menu_id']) ? $_GET['menu_id'] : 1;
$menuInstance = new WPRESTO_MENU();
$menu = $menuInstance->get_by_id($menuId);

$categoryInstance = new WPRESTO_Category();
$categories = $categoryInstance->get_all_actived_by_menu($menuId);
//    print_r($categories);
$lang = 'cn';
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
if ($menu['multi_lang'] == "1"){
  $lang = isset($_GET['lang']) ? $_GET['lang'] : 'cn';
}
//var_dump($menu);
//echo $menu['multi_lang'];
?>
<div class="front-view">
<div class="fixed header">
  <div class="wrap">
    <div class="logo">
      <?php if($lang == "en"){ ?>
        <div><?php echo $menu['shop_name']; ?> - <?php echo $menu['name']; ?></div>
      <?php }else{ ?>
        <div><?php echo $menu['shop_name_cn']; ?> - <?php echo $menu['name_cn']; ?></div>
      <?php } ?>
    </div>
    <?php if($menu['multi_lang'] == 1){ ?>
    <div class="language">
      <?php if($lang == "en"){ ?>
        <a class="waves-effect waves-red btn-flat" href="<?php echo site_url(); ?>/shop-info/?lang=cn&menu_id=<?php echo $menuId; ?>">中文</a>&nbsp;&nbsp;
      <?php }else{ ?>
        <a class="waves-effect waves-red btn-flat" href="<?php echo site_url(); ?>/shop-info/?lang=en&menu_id=<?php echo $menuId; ?>">EN</a>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</div>

<div class="row outer-view">
  <div class="">
    <div class="wrap">
      <div class="col s12">
        <div class="title">
          <?php if($lang == "en"){ ?>
            SHOP INFO
          <?php }else{ ?>
            店铺信息
          <?php } ?>
        </div>
      </div>
      <div class="map col s12">
        <iframe frameborder="0" style="border:0"
          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBMwzNak7PXmHgJOGPSVwSlr50N6WK1Cvg&q=<?php echo str_replace(' ', '+', $menu['address']); ?>" allowfullscreen>
        </iframe>
      </div>
      <div class="row">
      <?php if($lang == "en"){ ?>
        <div class="address">
          <div class="col s3">
            Address：
          </div>
          <div class="col s9">
            <?php echo $menu['address']; ?>
          </div>
        </div>
        <div class="phone">
          <div class="col s3">
            Phone：
          </div>
          <div class="col s9">
            <?php echo $menu['phone']; ?>
          </div>
        <div class="open-time">
          <div class="col s3">
            Open Time：
          </div>
          <div class="col s9">
            <?php echo $menu['open_time']; ?>
          </div>
      <?php }else{ ?>
          <div class="address">
            <div class="col s3">
              地址：
            </div>
            <div class="col s9">
              <?php echo $menu['address']; ?>
            </div>
          </div>
          <div class="phone">
            <div class="col s3">
              电话：
            </div>
            <div class="col s9">
              <?php echo $menu['phone']; ?>
            </div>
            <div class="open-time">
              <div class="col s3">
                营业时间：
              </div>
              <div class="col s9">
                <?php echo $menu['open_time']; ?>
              </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="menu-bottom col s12 fixed shop-info">
  <div class="row no-gutters text-center">
    <?php if($lang == "en"){ ?>
      <div class="shop-info col s6">
        <a href="<?php echo site_url(); ?>/menu-view?menu_id=<?php echo $menuId; ?>&lang=en">MENU</a>
      </div>
      <div class="shops col s6">
        SHOPS
        <div class="menu-dropup col s6">
          <ul>
            <?php
            $allMenuInstance = new WPRESTO_MENU();
            $menus = $allMenuInstance->get_all();
            foreach ($menus as $singleMenu){ ?>
              <li>
                <a href="<?php echo site_url(); ?>/menu-view?lang=en&menu_id=<?php echo $singleMenu->id; ?>">
                  <?php echo $singleMenu->name; ?>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    <?php }else{ ?>
      <div class="shop-info col s6">
        <a href="<?php echo site_url(); ?>/menu-view?lang=cn&menu_id=<?php echo $menuId; ?>">菜单</a>
      </div>
      <div class="shops col s6">
        所有店铺
      </div>
      <div class="menu-dropup col s6">
        <ul>
          <?php
          $allMenuInstance = new WPRESTO_MENU();
          $menus = $allMenuInstance->get_all();
          foreach ($menus as $singleMenu){ ?>
            <li>
              <a href="<?php echo site_url(); ?>/menu-view?lang=cn&menu_id=<?php echo $singleMenu->id; ?>">
                <?php echo $singleMenu->name_cn; ?>
              </a>
            </li>
          <?php } ?>
        </ul>
      </div>
    <?php } ?>
  </div>
</div>
</div>
<script>
    jQuery(document).ready(function($){
        $('.shops').click(function(e){
            if ( $( ".menu-dropup" ).is( ":hidden" ) ) {
                $( ".menu-dropup" ).slideDown();
            } else {
                $( ".menu-dropup" ).slideUp();
            }
        });
    });
</script>
<?php
get_footer('view');
?>
