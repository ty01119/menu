<?php
   /** 
     * Displays HTML Menu on frontend.
     */
    function wpresto_display_menu( $atts, $category_ids='all' ){
      global $wpdb;
      $menu_id = $atts['menu_id'];

      /* Get Menu, Categories, and Items */
      $menu = WPRESTO_Menu::get_by_id($menu_id);
     
      if(empty($menu['id'])) return "Menu not found.";

      //$categories = WPRESTO_Category::get_all((int) $menu['id'], $category_ids);
      $items = WPRESTO_Item::get_all((int) $menu['id']);

      $optshowImg = get_option('wpresto_showImg',1);
      $optImgPos = get_option('wpresto_ImgPos');
	  $optPriceUnit = get_option("wpresto_PriceUnit");
     
      /* Build HTML Output for menu */  
       //$m .= "<div class='wpresto_bootstrap'>";
        $m .= "<div class=\"wpresto_panel-group\" id=\"accordion". $menu_id . "\">\r\n";
        $m .= "<div class=\"wpresto_panel panel-default\">\r\n";
        $m .= "<div class=\"wpresto_panel-heading\">\r\n";
        //$m .= "<div class='menu_manager'>"."\r\n";
        $m .= "<a data-toggle='collapse' data-parent='#accordion".$menu_id."' href='#" .$menu_id."'>";
        if($menu['show_title']){
            $m .= '<h1 class="menutitle">'.$menu['name'].'</h1>'."\r\n";
        }

        $m .= "</a></div>"."\r\n";
        $m .= "<div id='".$menu_id."' class='wpresto_panel-collapse collapse in'>\r\n";
        $m .= "<div class='wpresto_panel-body'>\r\n";
        $m .= '<p class="menu_desc">'.nl2br($menu['description']).'</p>'."\r\n";


         /* loop Category */        
	   	global $wpdb;
	   	//$sql = 'SELECT category_id,display_order FROM '.WPPRESTO_ITEM_DB.' where menu_id="'.$menu_id.'" group by category_id order by display_order';
	   	$sql = 'SELECT * FROM '.WPPRESTO_CATEGORY_DB.' where menu_id='.$menu_id.' ORDER BY display_order';
	    $categories = stripslashes_deep($wpdb->get_results($sql));
        //echo "menu: " .$menu_id;
        //print_r($categories);
        
        foreach($categories as $c)
        {
			//$cat = $wpdb->get_results('SELECT name, description FROM '.WPPRESTO_CATEGORY_DB.' WHERE id="'.$c->category_id.'" LIMIT 1', ARRAY_A);
			//$cat = $cat[0];
			if($c->active!=0)
			{
			//$m .= '<div class="wpresto_panel-body"><span class="wpresto-catergory-title">'.$cat['name'].'</span>';
			$m .= '<div class="wpresto_panel-body"><span class="wpresto-catergory-title">'.$c->name.'</span>';
			$m .= '<div class="wpresto-menu-line"></div>';
			//$m .= ' - <span class="wpresto-catergory-desc">'.$cat['description'].'</span><br>';
		          /* loop items */
		          $items = WPRESTO_Item::get_all_by_cat($c->id,$menu_id);
		          foreach($items as $t) {
		            if($t->active==1){
		            $m .= '<div class="media">';
		             if($optshowImg==1) {
		                if($optImgpos == "right") { 
		                    $m .= '< class="pull-right" href="#">';
		                } else {
		                    $m .= '<span class="pull-left" href="#">';
		                }
		                    $m .=    '<img class="media-object" src="'.$t->image.'" width=120>';
							/*if($t->show_price==1) 
							{
								$m .= '<a class="pull-right" href="#">';
								$m .= '<span class="wpresto-menu-price">$'.$t->price.'</span>';
								$m .= '</a>';
							}*/
							$m .= '</span>';
		             } else {
	  	                 //if($t->show_price==1)  $m .=            '<span class="pull-right"><span class="wpresto-menu-price">'.$optPriceUnit.$t->price.'</span></span><br>';
		             }
		             $m .=     '<div class="media-body">';
 					 $m .=         '<div class="wpresto-menu-head">';
		             $m .=            '<span class="wpresto-menu-title">'.$t->name.'</span>';
						if($t->show_price==1) {			 		 
							 $m .=            '<span class="pull-right"><span class="wpresto-menu-price">'.$optPriceUnit.$t->price.'</span></span><br>';
							 //$m .=            '<div class="wpresto-menu-line"></div>'; 
						}
					 $m .=         '</div>';
		             $m .=        '<span class="wpresto-menu-details">'.nl2br($t->description).'</span>';
		             $m .=     '</div>';
		             $m .= '</div>';
		            }    
		          }
				/* loop items */
    		$m .= "</div>";
			$m .= '<br><br>';
    		//$m .= "<hr style='width:100%'>";
    		//$m .= "<div>&nbsp;</div>";
    		}
		 }
		/* loop Category */ 
		 if($_GET['wpresto-action']!="print") { 	    
	          if($menu['show_link']){
	          	//$m .= 'menu od = ' . $menu_id;
	            $m .= '<br><p class="print"><a href="?wpresto-action=print&menu_id='.$menu_id.'" target="_blank">Print</a></p>';
              }
         }



          $m .= '</div>';
          $m .= '</div>';
          $m .= '</div>';
          $m .= '</div>';
          //$m .= '</div>';
          return $m;
    }

?>