<?php
class WPRESTO_Item{
 
  /*public $id, $name, $description;*/
  private $parent_menus;

  public function __construct($id = 'ALL') { 
    $this->id = '';
    $this->name = 'New';
    $this->name_cn = 'New';
    $this->description = '';
    $this->description_cn = '';
    $this->price = '0.00';
    $this->second_price = '0.00';
    $this->image = 'pluginurl/images/defaultImg.png';
    $this->show_price = 1;
    $this->active = 1;
    $this->display_order = 0;
    $this->category_id = 0;
    if(is_numeric($id)) $this->load_item($id);
  }

  /* @return all items in the DB */
  public static function get_all($menu_id = 'ALL'){
    global $wpdb;
    $where = is_numeric($menu_id)? 'WHERE menu_id="'.$menu_id.'"' : '';
    $sql = "SELECT ".WPPRESTO_ITEM_DB.".* FROM ".WPPRESTO_ITEM_DB." $where ORDER BY display_order DESC";
    $items = $wpdb->get_results($sql);
    return stripslashes_deep($items);
  }

  public static function get_all_by_cat($cat_id = 'ALL', $menu_id){
    global $wpdb;
    $where = 'WHERE category_id="'.$cat_id.'" AND menu_id="'.$menu_id.'"';
    $sql = "SELECT ".WPPRESTO_ITEM_DB.".* FROM ".WPPRESTO_ITEM_DB." $where ORDER BY display_order DESC";
    $items = $wpdb->get_results($sql);
    return stripslashes_deep($items);
  }

  public static function get_all_actived_by_cat($cat_id = 'ALL', $menu_id){
    global $wpdb;
    $where = 'WHERE category_id="'.$cat_id.'" AND menu_id="'.$menu_id.'" AND active=1';
    $sql = "SELECT ".WPPRESTO_ITEM_DB.".* FROM ".WPPRESTO_ITEM_DB." $where ORDER BY display_order DESC";
    $items = $wpdb->get_results($sql);
    return stripslashes_deep($items);
  }

  private function load_item($id){
    global $wpdb;
    $item = $wpdb->get_results('SELECT * FROM '.WPPRESTO_ITEM_DB.' WHERE id="'.$id.'" LIMIT 1', ARRAY_A);
    $item = $item[0];
    if(empty($item)) return '';
    $this->id = $item['id'];
    $this->name = stripslashes($item['name']);
    $this->name_cn = stripslashes($item['name_cn']);
    $this->description = stripslashes($item['description']);
    $this->description_cn = stripslashes($item['description_cn']);
    $this->price = stripslashes($item['price']);
    $this->second_price = stripslashes($item['second_price']);
    $this->show_price = stripslashes($item['show_price']);
    $this->icon_class = stripslashes($item['icon_class']);
    $this->active = stripslashes($item['active']);
    $this->display_order = stripslashes($item['display_order']);
    $this->menu_id = stripslashes($item['menu_id']);
    $this->category_id = stripslashes($item['category_id']);
    $this->image = stripslashes($item['image']);
  }
  
  /* Loads parent menus when load_all is requested */
  public function load_parent_menus(&$items){
    $menu_ids = array();
    foreach($items as $i){
      $menu_ids[] = $i->menu_id;
    }
    if(empty($menu_ids)) return '';
    $where = ' WHERE id='.implode(' OR id=',$menu_ids);
    global $wpdb;
    $this->parent_menus = $wpdb->get_results('SELECT * FROM '.WPPRESTO_MENU_DB.' '.$where.' ORDER BY id');
  }
  
  /* Get parent menu */
  public function get_parent_menu($menu_id){
    foreach($this->parent_menus as $m){
      if($m->id == $menu_id) return $m;
    }
  }

  /* Saves item on admin update */
  public static function update($item){
    global $wpdb;
    $item['display_order'] = empty($item['display_order'])? 0 : $item['display_order'];
    $wpdb->update(WPPRESTO_ITEM_DB, $item, array('id'=>$item['id']));
  }

  /* Creates item in DB */
  public static function create($item){
    global $wpdb;
    $item['display_order'] = empty($item['display_order'])? 0 : $item['display_order'];
    $wpdb->insert(WPPRESTO_ITEM_DB, $item);
    return $wpdb->insert_id;
  }

  /* Deletes item from DB */
  public static function destroy($item_id){
    global $wpdb;
    if(empty($item_id)) return '';
    $wpdb->query('DELETE FROM '.WPPRESTO_ITEM_DB.' WHERE id="'.$item_id.'"'); 
  }

  public function destroySingle($item_id, $menu_id){
    global $wpdb;
    if(empty($item_id)) return '';
    $sql = 'DELETE FROM '.WPPRESTO_ITEM_DB.' WHERE id="'.$item_id.'" AND menu_id="'.$menu_id.'"';
    $wpdb->query($sql); 
    return $sql;
  }
}
?>
