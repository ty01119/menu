<?php
/**
 *  Setup database tables on plugin activation
 */
function WPPRESTO_install_plugin() {
  global $wpdb;

  $sql = "CREATE TABLE ".WPPRESTO_MENU_DB." (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` text,
          `name_cn` text,
          `show_title` tinyint(4) NOT NULL DEFAULT '1',
          `description` text NOT NULL,
          `description_cn` text,
          `shop_name` text,
          `shop_name_cn` text,
          `address` text,
          `phone` text,
          `open_time` text,
          `multi_lang` int(11) DEFAULT '0',
          `show_link` tinyint(4) NOT NULL DEFAULT '1',
          `category_id` int(11) NOT NULL DEFAULT '0',
          `rss2` tinyint(4) NOT NULL DEFAULT '1',
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          UNIQUE KEY id (id)
        )DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $wpdb->query($sql);
  $sql = "CREATE TABLE ".WPPRESTO_CATEGORY_DB." (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` text,
          `name_cn` text,
          `description` text NOT NULL,
          `description_cn` text,
          `layout` text NOT NULL,
          `active` tinyint(4) NOT NULL DEFAULT '1',
          `show_title` tinyint(4) NOT NULL DEFAULT '1',
          `show_description` tinyint(4) NOT NULL DEFAULT '1',
          `display_order` int(11) NOT NULL,
          `menu_id` int(11) NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          UNIQUE KEY id (id)
        )DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $wpdb->query($sql);
  $sql = "CREATE TABLE ".WPPRESTO_ITEM_DB." (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` text,
          `name_cn` text,
          `description` text NOT NULL,
          `description_cn` text,
          `price` text NOT NULL,
          `second_price` text NOT NULL,
          `image` text NOT NULL,
          `icon_class` text NOT NULL,
          `show_price` tinyint(4) NOT NULL DEFAULT '1',
          `active` tinyint(4) NOT NULL DEFAULT '1',
          `display_order` int(11) NOT NULL,
          `menu_id` int(11) NOT NULL,
          `category_id` int(11) NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          UNIQUE KEY id (id) 
        )DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
    $wpdb->query($sql);
	// Create first menu upon activation
	add_option('wpresto_showImg',1);
	add_option("wpresto_PriceUnit","$");
	
}
?>
