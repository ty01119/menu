<div class="wrap wpresto">
  <h2 class="left">Restaurant Menu Manager - Edit Menu</h2>
  <div class="clear"></div>
  <hr />

<?php

$item_id = $_GET['item_id'];
$menu_id = $_GET['menu_id'];

if(isset($_POST['update_menu'])) {
	$updatedMenu = $_POST['wpresto'];
	if (!isset($_POST['wpresto']['show_title'])) $updatedMenu['show_title']=0;  
	if (!isset($_POST['wpresto']['show_link'])) $updatedMenu['show_link']=0;  
	$menu2 = new WPRESTO_Menu();
	$new = $menu2->update($updatedMenu);
    echo wpresto_alert_msg("Menu <b>" . $updatedMenu['name'] . "</b> successfully updated! ");

}

//wpresto_save_menu();
$menu = new WPRESTO_Menu((int) $_GET['menu_id']);

?>



  <p class="wpresto-breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <a href="admin.php?page=main_menu&action=edit_single_menu&menu_id=<?php echo $menu->id;?>"><?php echo $menu->name; ?></a>
  </p>

  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <table class="form-table">
      <tbody>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="<?php echo $menu->name_cn;?>" class="regular-text">
              <span class="description">中文名称</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="<?php echo $menu->name;?>" class="regular-text">
              <span class="description">Display name for this menu.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_title]">Show title on frontend?</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_title]" value="1" <?php echo ($menu->show_title == 1) ? 'checked' : '';?>/>
            <span class="description">Display menu title?</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"><?php echo $menu->description_cn;?></textarea>
                <span class="description">中文描述.</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"><?php echo $menu->description;?></textarea>
              <span class="description">Display description for this menu.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="shortcode">Shortcode</label></th>
          <td>
              <input name="shortcode" type="text" class="regular-text" 
                     value='[WP_RestoMenu menu_id="<?php echo htmlentities($menu->id);?>"]' readonly="readonly" />
              <span class="description">Put this shortcode into a Page or Post for this menu to appear.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="print_view_url">Print View URL</label></th>
          <td>
              <a href="<?php echo site_url("?wpresto-action=print&menu_id=".$menu->id);?>" target="_blank"><?php echo site_url("?wpresto-action=print&menu_id=".$menu->id);?></a>
              <br />
              <span class="description">URL to the printer friendly view of this menu.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_link]">Show print link?</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_link]" value="1" <?php echo ($menu->show_link == 1) ? 'checked' : '';?>/>
            <span class="description">Shows the link to the printer friendly version of the menu on the frontend.</span>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><label for="wpresto[rss2]">Turn on RSS?</label></th>
          <td>
            <input type="hidden" name="wpresto[rss2]" value="0" />
            <input type="checkbox" name="wpresto[rss2]" value="1" <?php echo ($menu->rss2 == 1) ? 'checked' : '';?> />
            <span class="description">
              Allow RSS feed of menu. RSS2 Feed URL:<br />
              <a href="<?php echo $menu->rss2_url;?>" target="_blank"><?php echo $menu->rss2_url;?></a>
            </span>
          </td>
        </tr>        
        

      </tbody>
    </table>

    <input type="hidden" name="wpresto[id]" value="<?php echo $menu->id;?>" />
    
    <div class="wpresto-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="update_menu" value="Save Menu" />&nbsp;
        <a class="button" href="<?php echo home_url(); ?>/menu-list">&laquo;back</a>&nbsp;

      </p>
    </div>

  </form>

</div>
