<div class="wrap wpresto">
  <h2 class="left">Restaurant Menu Manager - Edit Items</h2>
  <div class="clear"></div>
  <hr />


<?php

$item_id = isset($_GET['item_id']) ? $_GET['item_id'] : '';
$menu_id = isset($_GET['menu_id']) ? $_GET['menu_id'] : '';

if(isset($_POST['update_item'])) {
    //die("iamge: " . $_POST['item_image']);
    //print_r($_FILES);//['wpresto']['item_image']);
	$updatedItems = $_POST['wpresto'];
	$updatedItems['menu_id'] = $_POST['menu_id'];

	if (!isset($_POST['wpresto']['active'])) $updatedItems['active']=0;  
	if (!isset($_POST['wpresto']['show_price'])) $updatedItems['show_price']=0;

    $currentImg = $_POST['current_item_image'];

    $uploaddir = WPPRESTO_PATH . "images/";
    $name = $_FILES['item_image']['name'];
    $ext = end((explode(".", $name))); 
    $fileName = "food-".$item_id.".".$ext;
    $uploadfile = $uploaddir . $fileName;
    $uploadOk = 0;

    if (move_uploaded_file($_FILES['item_image']['tmp_name'], $uploadfile)) {
		$uploadOk = 1;
	} else {
		//echo wpresto_alert_msg("There was an error in uploading");;
	}
    if($_FILES['item_image']['name']!='')   
    {
        if($uploadOk==1) {
            $updatedItems['image'] = WPPRESTO_PLUGIN_URL."/images/".$fileName;
        } else {
            if ($_POST['wpresto']['image']=='') $updatedItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
        }
    } else {
        $updatedItems['image'] = $currentImg;
    }
    //if ($updatedItems['image']=='') $updatedItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
	$item2 = new wpresto_Item();
	$new = $item2->update($updatedItems);
	echo wpresto_alert_msg("Item <b>" . $_POST['wpresto']['name'] . "</b> successfully updated! ");
    //print_r($updatedItems);
    //print_r($_FILES);
} 

$item = new wpresto_Item((int) $_GET['item_id']);
$menu = new wpresto_Menu((int) $item->menu_id);

$category = new wpresto_Category();
$categories = $category->get_all();

$menuList = new WPRESTO_MENU();
$menus = $menuList->get_all();

?>

<script type="text/javascript">
jQuery(document).ready(function($){
       jQuery("#upload_image_file").change(function($) {
           //alert("asd");
           readURL(this);
       });
   });

    function readURL(input) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();
               reader.onload = function(e) {
                   $('#previewHolder').attr('src', e.target.result);
                   $('#previewHolder').attr('width', 200);
                   
               };
               reader.readAsDataURL(input.files[0]);
           }
       }

 //  });

</script>

<p class="wpresto-breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <?php if($_GET['referrer']=='fooditems') { ?> 	
		<a href="admin.php?page=fooditems">Item List</a> &raquo; 
    <?php } else { ?>
        <a href="admin.php?page=main_menu&action=edit_item&menu_id=<?php echo $menu->id;?>"><?php echo $menu->name; ?></a>&raquo;
        <a href="admin.php?page=main_menu&action=edit_single_item&item_id=<?php echo $item_id;?>"><?php echo $item->name; ?></a>&raquo;
    <?php } ?>
    	<a href="">Edit Item</a>
  </p>

  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
    <table class="form-table">
      <tbody>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="<?php echo $item->name_cn;?>" class="regular-text">
              <span class="description">名称</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="<?php echo $item->name;?>" class="regular-text">
              <span class="description">Display name for this item.</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"><?php echo $item->description_cn;?></textarea>
                <span class="description">中文描述</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"><?php echo $item->description;?></textarea>
              <span class="description">Display description for this item.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[menu_id]">Link to Menu</label></th>
          <td>
            <select name="menu_id" class="regular-text code">
              <?php foreach($menus as $m):?>
                <option value="<?php echo $m->id;?>" <?php echo ($item->menu_id == $m->id)? 'selected' : '';?>><?php echo $m->name;?></option>
              <?php endforeach;?>
              <option value="0" <?php echo ($item->menu_id == "0")? 'selected' : '';?>>None (hidden)</option>
            </select>
            <span class="description">Link this item to a menu</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row">Upload Image</th>
          <td>
            <label for="upload_image">
              <input id="upload_image" type="text" size="36" name="wpresto[image]" value="" />
              <span class="description">Enter an URL or upload an image for this item.</span><br>
              <input type="hidden" name="MAX_FILE_SIZE" value="4194304" /> 
              <input id="current_image_file" type="hidden" value="<?php echo $item->image; ?>" name="current_item_image"/>
              <input id="upload_image_file" type="file" value="Upload Image" name="item_image"/>
              <span class="description">Upload an image for this item.</span><br>
              <img id="previewHolder" src="<?php echo $item->image; ?>" alt="your image" width=200/>
            </label>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[price]">Price</label></th>
          <td><input name="wpresto[price]" type="text" value="<?php echo $item->price;?>" class="regular-text">
              <span class="description">Displayed Price. Include denomination if desired.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[second_price]">Secondary Price</label></th>
          <td><input name="wpresto[second_price]" type="text" value="<?php echo $item->second_price;?>" class="regular-text">
              <span class="description">Use to display half and full order prices, or price with special side etc.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_price]">Show Price</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_price]" value="1" <?php echo ($item->show_price == 1) ? 'checked' : '';?>/>
            <span class="description">Display the price for this item.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[category_id]">Category</label></th>
          <td>
            <select name="wpresto[category_id]" class="regular-text code">
              <?php foreach($categories as $cat):?>
                <option value="<?php echo $cat->id;?>" <?php echo ($item->category_id == $cat->id)? 'selected' : '';?>><?php echo $cat->name;?></option>
              <?php endforeach;?>
              <option value="0" <?php echo ($item->category_id == "0")? 'selected' : '';?>>None (hidden)</option>
            </select>
            <span class="description">Category for this item in this menu. Controls layout of items. Setup Categories and then items.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[active]">Active</label></th>
          <td>
            <input type="checkbox" name="wpresto[active]" value="1" <?php echo ($item->active == 1) ? 'checked' : '';?>/>
            <span class="description">Display this item.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[display_order]">Display Order</label></th>
          <td><input name="wpresto[display_order]" type="text" value="<?php echo $item->display_order;?>" class="regular-text">
              <span class="description">Order in which this item is displayed to users.</span>
          </td>
        </tr>

      </tbody>
    </table>

    <br />

    <input type="hidden" name="wpresto[id]" value="<?php echo $item->id;?>" />

    <div class="wpresto-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="update_item" value="Save Item" />
        <a class="button" href="admin.php?page=main_menu&action=edit_item&menu_id=<?php echo $item->menu_id; ?>">&laquo;back to Items</a>&nbsp;
        <span>
          <a class="button" href="admin.php?page=main_menu&action=create_item&menu_id=<?php echo $item->menu_id; ?>">+ Add New Item</a>&nbsp;
        </span>&nbsp;
      </p>
    </div>


  </form>

</div>
