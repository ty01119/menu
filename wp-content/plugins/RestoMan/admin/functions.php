<?php
defined('WPPRESTO_PATH') or die();

/**
 *  Loads admin page from request via GET
 *  
 *  @echo admin php file
 */
function wpresto_admin_menulist(){
   $admin_page = empty( $_GET[WPPRESTO_ADMIN_PARSE] ) ? '' : $_GET[WPPRESTO_ADMIN_PARSE];
   $action = $_GET['action'];
   
	switch ($action) {
		case '':
			include "menu-list.php";		
		break;

		case 'create':
			include "new-menu.php";					
		break;

		case 'edit_item':
			include "edit-menu-items.php";					
		break;		

		case 'create_item':
			include "create-menu-items.php";					
		break;		
		
		case 'new_item':
			include "new-items.php";					
		break;

		case 'edit_single_item':
			include "edit-item.php";					
		break;		

		case 'edit_single_menu':
			include "edit-menu.php";					
		break;	
		
		case 'create_category':
			include "new-category.php";					
		break;
        
		case 'edit_category':
			include "edit-category.php";					
		break;

		case 'menu_category':
			include "menu-category-list.php";					
		break;
		
		case 'create_menu_category':
			include "new-menu-category.php";					
		break;
		
		
        case 'settings':
            include "settings.php";
        break;

	}
   
}

function wpresto_admin_menuitem()
{
	echo "menu";
}

function wpresto_admin_category()
{
	include "category-list.php";
}

function wpresto_admin_fooditems()
{
	include "item-list.php";	
}

function wpresto_admin_settings()
{
	include "settings.php";
}

function wpresto_alert_msg($msg)
{
      $s = '<div id="message" class="updated below-h2">
          <p>'.$msg.'</p>
        </div>';
       return $s;
}


function getTotalItems($menu_id)
{
    global $wpdb;
    $sql = "SELECT count(*) as t FROM ".WPPRESTO_ITEM_DB." WHERE menu_id = " . $menu_id ;
    $items = $wpdb->get_results($sql,ARRAY_A);
    return $items[0]['t'];
}

function getTotalCategory($menu_id)
{
    global $wpdb;
    $sql = "SELECT count(*) as t FROM ".WPPRESTO_CATEGORY_DB." WHERE menu_id = " . $menu_id ;
    $items = $wpdb->get_results($sql,ARRAY_A);
    return $items[0]['t'];
}



?>
