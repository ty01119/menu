<?php


if(isset($_POST['save_options']))
{
	$optshowImg = "";
	if (!isset($_POST['wpresto']['option1'])) {
		$optshowImg=0;  
	} else {
		$optshowImg=1;
	}
	update_option("wpresto_showImg",$optshowImg);
	update_option("wpresto_ImgPos",$_POST['wpresto']['position']);
	update_option("wpresto_PriceUnit",$_POST['wpresto']['priceunit']);
	
}

$optshowImg = get_option("wpresto_showImg",1);
$optImgPos = get_option("wpresto_ImgPos");
$optPriceUnit = get_option("wpresto_PriceUnit");

$custom_css = stripslashes_deep(  get_option('wprmm_custom_css') );
$custom_print_css = stripslashes_deep( get_option('wprmm_custom_print_css') );

?>
<div class="wrap wprmm wprmm_settings">
  <h2 class="left">Restaurant Menu Manager - Global Settings</h2>
  <div class="clear"></div>
  <hr />

  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
      
	  <h3>Menu display settings</h3>
      Here you can set the menu to show or hide image on the page
      <br><br>
	  <input type='checkbox' name='wpresto[option1]' value="1" <?php echo ($optshowImg==1) ? 'checked' : ''; ?>>Show menu images 
	  <br><br>
	  Menu Image position:
        <select name="wpresto[position]">
            <option value="left" <?php echo ($optImgPos=="left") ? 'selected' : ''; ?>>Left</option>
            <option value="right" <?php echo ($optImgPos=="right") ? 'selected' : ''; ?>>Right</option>
        </select>  
		
	  <br><br>
	  Menu Price Unit:
		<input type='test' name='wpresto[priceunit]' value="<?php echo $optPriceUnit; ?>">
      </div>

  <p>
  <br>
  <input class="button-primary" class="left" type="submit" name="save_options" value="Save Options" />&nbsp;
  </p>
</div>

