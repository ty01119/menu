<div class="wrap WPPRESTO">
  <h2 class="left">Restaurant Menu Manager - Setup Menus</h2>
  <div class="clear"></div>
  <hr />
<?php 

if(isset($_POST['wpresto_delete']['menu']))
{
	$del_menu_id = $_POST['wpresto_delete']['menu_id'];
	$menu1 = new WPRESTO_Menu();
	$menu1->destroy($del_menu_id);
	echo wpresto_alert_msg("Menu <b>" . $_POST['wpresto_delete']['menu_name'] . "</b> successfully deleted! ");

}


$menuList = new WPRESTO_MENU();
$menus = $menuList->get_all();

?>


  <table class="widefat">
  <thead>
    <tr>
      <th>Name</th>
      <th>名称</th>
      <th>Edit Menu</th>
      <th>Shortcode</th>
      <th>Edit Menu Category Set</th>
      <th>Edit Menu Items</th>
      <th>Print Friendly</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($menus as $m): ?>
     <tr>
       <td><strong><?php echo $m->name; ?></strong></td>
       <td><strong><?php echo $m->name_cn; ?></strong></td>
       <td><a href="admin.php?page=main_menu&action=edit_single_menu&menu_id=<?php echo $m->id; ?>"><?php echo $m->name; ?></a></td>
       <td>
         <code>[WP_RestoMenu menu_id="<?php echo htmlentities($m->id);?>"]</code>
       </td>
       <td><a href="admin.php?page=main_menu&action=menu_category&menu_id=<?php echo $m->id; ?>">Edit Categories (<?php echo getTotalCategory($m->id);?>)</a></td>
       <td><a href="admin.php?page=main_menu&action=edit_item&menu_id=<?php echo $m->id; ?>">Edit Items (<?php echo getTotalItems($m->id);?>)</a></td>
       <td>
         <a href="<?php echo site_url();?>/?wpresto-action=print&menu_id=<?php echo $m->id; ?>" target="_blank">Print Menu</a>
       </td>
       <td>
         <form method="post" action="#">
           <input type="submit" class="button" name="wpresto_delete[menu]" value="Delete" onclick="return confirm('Are you sure you want to delete <?php echo $m->name; ?> menu?.')" />
           <input type="hidden" name="wpresto_delete[menu_id]" value="<?php echo $m->id;?>" />
           <input type="hidden" name="wpresto_delete[menu_name]" value="<?php echo $m->name;?>" />
         </form>
       </td>
     </tr>
    <?php endforeach; ?>
  </tbody>
  </table>

  <div class="WPPRESTO-admin-nav">
    <p>
      <a class="button-primary" href="admin.php?page=main_menu&action=create">+ Create New Menu</a>&nbsp;
    </p>
  </div>

</div>
